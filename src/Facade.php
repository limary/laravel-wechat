<?php
namespace Sinta\LaravelWeChat;

use Illuminate\Support\Facades\Facade as LaravelFacade;

class Facade extends LaravelFacade
{
    /**
     * 默认为 Server.
     *
     * @return string
     */
    public static function getFacadeAccessor()
    {
        return 'wechat.official_account';
    }

    /**
     * @return \Sinta\Wechat\OfficialAccount\Application
     */
    public static function officialAccount()
    {
        return app('wechat.official_account');
    }

    /**
     * @return \Sinta\Wechat\Work\AgentFactory
     */
    public static function work()
    {
        return app('wechat.work');
    }

    /**
     * @return \Sinta\Wechat\Payment\Application
     */
    public static function payment()
    {
        return app('wechat.payment');
    }

    /**
     * @return \Sinta\Wechat\MiniProgram\Application
     */
    public static function miniProgram()
    {
        return app('wechat.mini_grogram');
    }

    /**
     * @return \Sinta\Wechat\OpenPlatform\Application
     */
    public static function openPlatform()
    {
        return app('wechat.open_platform');
    }
}
