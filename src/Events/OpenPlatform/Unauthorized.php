<?php
namespace Sinta\LaravelWeChat\Events\OpenPlatform;

/**
 * 不授权
 *
 * Class Unauthorized
 * @package Sinta\LaravelWeChat\Events\OpenPlatform
 */
class Unauthorized
{
    public $payload;

    /**
     * Create a new event instance.
     *
     * @param array $payload
     */
    public function __construct(array $payload)
    {
        $this->payload = $payload;
    }
}
