<?php
namespace Sinta\LaravelWeChat\Events\OpenPlatform;

/**
 * 授权更新
 *
 * Class UpdateAuthorized
 * @package Sinta\LaravelWeChat\Events\OpenPlatform
 */
class UpdateAuthorized
{
    public $payload;

    /**
     * Create a new event instance.
     *
     * @param array $payload
     */
    public function __construct(array $payload)
    {
        $this->payload = $payload;
    }
}
