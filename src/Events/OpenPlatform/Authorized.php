<?php
namespace Sinta\LaravelWeChat\Events\OpenPlatform;

/**
 * 授权
 *
 * Class Authorized
 * @package Sinta\LaravelWeChat\Events\OpenPlatform
 */
class Authorized
{
    public $payload;

    public function __construct(array $payload)
    {
        $this->payload = $payload;
    }
}
