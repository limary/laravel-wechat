<?php
namespace Sinta\LaravelWeChat\Controllers;

use Sinta\Wechat\OpenPlatform\Application;
use Sinta\Wechat\OpenPlatform\Server\Guard;
use Event;
use Sinta\LaravelWeChat\Events\OpenPlatform as Events;


/**
 * 平台控制器
 *
 * Class OpenPlatformController
 * @package Sinta\LaravelWeChat\Controllers
 */
class OpenPlatformController
{
    /**
     * Register for open platform.
     *
     * @param \Sinta\Wechat\OpenPlatform\Application $application
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Application $application)
    {
        $server = $application->server;

        $server->on(Guard::EVENT_AUTHORIZED, function ($payload) {
            Event::fire(new Events\Authorized($payload));
        });
        $server->on(Guard::EVENT_UNAUTHORIZED, function ($payload) {
            Event::fire(new Events\Unauthorized($payload));
        });
        $server->on(Guard::EVENT_UPDATE_AUTHORIZED, function ($payload) {
            Event::fire(new Events\UpdateAuthorized($payload));
        });

        return $server->serve();
    }
}
